﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;

namespace Program
{
    class Program
    {
        // NOTES: FIND A WAY OF FINDING OUT WHETHER THE SEARCH EXISTS OR NOT
        
        /* NOTES: SEARCH VARIATIONS

            IF THE USER SEARCHES FOR DEB instead of DEBRA
                Results of people who have deb should show

                So IF NO RESULTS ARE FOUND
                    ASSUME SEARCH IS A PARTIAL PHRASE
                        SEARCH FOR STRINGS THAT HAVE THIS PHRASE WITHIN THEM IN DIFFERENT PLACES
                            
            So a search for deb should return

                DEBRA
                DEBENA
                ANDEB
                JAYDEB
                EDEBURY

            As these strings stil have the char in them

            IF SEARCH NOT FOUND
                MOVE


        
        */
        static void Main(string[] args)
        {
            // NOTE: USER CANNOT USE THE '-' in the naming of there file as it is the filla for the spaces and for adding extra chars to make two strings the same

            // Get the information in the notepad that stores the scores
            string[] Collection = File.ReadAllLines("SearchPractise.txt");

            Stopwatch  SearchSpeed = new Stopwatch();

            SearchSpeed.Start();
            int SearchStringPosition = findBinarySearch(Collection,"Elihu-Jermaine",0,Collection.Length);
            SearchSpeed.Start();

            Console.WriteLine("Found Results in " + SearchSpeed.Elapsed.ToString());

            string[] Results = GetSorroundingResults(Collection,SearchStringPosition,20);
            
            for (int i = 0; i < Results.Length - 1; i++)
            {
                // Remove - and add the spaces
                Results[i].Replace('-',' ');

                Console.WriteLine(i + ". " + Results[i]);
            }
            // Show the top 20 results


    
        }

        public static int X = 0;
        
        public static int findBinarySearch(string[] Collection, string Search, int TopBoundry, int BottomBoundry)
        {
            // to find middle, TOPBOUNDRY + (BOTTOMBOUNDRY - TOPBOUNDRY)
            int MiddlePosition = (BottomBoundry + TopBoundry)/2; // the middle value is equal to the bottom
            
            // Remove the dashes added  in the previous search as the function gets called multiple times and the extra dashes increase the length of the search string which also means the middle phrase has to be uneccessarily increased even if its actually the sae size of the search string
            do
            {
                Search = Search.TrimEnd('-');
            } while (Search.Substring(Search.Length - 1,1) == "-");    

            // Replace spaces with - in the search text 
            Search = Search.Replace(' ','-');
              // Make both strings upper case
            Search = Search.ToUpper();
            // Do the same for the middle phrase
            string MiddlePhrase = Collection[MiddlePosition].Replace(' ','-');
            // Make this string uppercase for the searching and char rating
            MiddlePhrase = MiddlePhrase.ToUpper();

            if (MiddlePosition == 2811)
            {
                
            }
            // Make two strings the same size
            // Check if they are the two strings are the same as they need to be 
            if (Search.Length < MiddlePhrase.Length)
            {
                // Store the string lengths because they change inside the for loop and therefore cannot be used as the forloops length
                int NumberOfCharsToBeAdded = MiddlePhrase.Length - Search.Length;
                for (int i = 0; i < NumberOfCharsToBeAdded; i++)
                {
                    Search = Search + '-';
                }
            }
            else if (Search.Length > MiddlePhrase.Length)
            {
                 // Store the string lengths because they change inside the for loop and therefore cannot be used as the forloops length
                int NumberOfCharsToBeAdded = Search.Length - MiddlePhrase.Length;
                for (int i = 0; i < NumberOfCharsToBeAdded; i++)
                {
                    MiddlePhrase = MiddlePhrase + '-';
                }
            }



            if (MiddlePhrase == Search) 
            {
                return MiddlePosition;
            }
            else
            {
                //long A = ValueString("01213");
                //long B = ValueString("001ZA");


                // Get the value for the searched string item
                //long SearchStringValue = ValueString(Search);

                //long MiddlePositionStringValue = ValueString(Collection[MiddlePosition]);

                if (FindBiggerString(Search,MiddlePhrase,0) == Search) // if the search string value is smaller it means that it is hiegher in the table as the further down a string goes the larger its value
                {
                    // List<string> RemoveItem = Collection.OfType<string>().ToList();

                    // for (int i = 0; i < Collection.Length / 2; i++)
                    // {
                     
                    //     // Remove the slot 
                    //     RemoveItem.RemoveAt(0);
                    // }
                    // Collection = RemoveItem.ToArray();

                    /*
                        As the middle position is not the required value also take it out
                        
                        ORIGINAL SEARCH AREA = 0 - 1000
                        MIDDLE WILL BE 500

                        IF MIDDLE IS TOO BIG
                            SEARCH AREA = 0 - 499
                        IF MIDDLE IS TOO SMALL
                            SEARCH AREA = 501 - 1000

                     */
                    // CHANGE BOTTOM BOUNDRY
                    TopBoundry = MiddlePosition + 1;

                    return findBinarySearch(Collection, Search,TopBoundry,BottomBoundry);

                } 
                else if (FindBiggerString(Search,MiddlePhrase,0) == MiddlePhrase)
                {
                    // remove all the positions below this value
                    // Remove the slot 
                    // List<string> RemoveItem = Collection.OfType<string>().ToList();

                    // for (int i = Collection.Length / 2; i < Collection.Length; i++)
                    // {
                    //     RemoveItem.RemoveAt(Collection.Length / 2);

                    // }
                    // Collection = RemoveItem.ToArray();

                    // Change the bottom boundry
                    BottomBoundry = MiddlePosition - 1;

                    return findBinarySearch(Collection, Search, TopBoundry,BottomBoundry);

                }

            }
           

            // IF THE TOP AND BOTTOM BOUNDRY ARE THE SAME IT SEEMS THAT THE SYSTEM ROUNDED TO THE WRONG VALUE AND EITHER A 1 DOWN OR 1 UP IS THE CORRECT VALUE
            Console.WriteLine("ITEM NOT FOUND");
            return 0;
        }
        public static string FindBiggerString(string StrA, string StrB, int CharToCompare)
        {
            
            if (ValueChar(StrA[CharToCompare]) > ValueChar(StrB[CharToCompare]))
            {
                return StrA;
            }
            else if(ValueChar(StrB[CharToCompare]) > ValueChar(StrA[CharToCompare]))
            {
                return StrB;   
            }
            else if (ValueChar(StrA[CharToCompare]) == ValueChar(StrB[CharToCompare]))
            {
                // Since the char are the same and therefore have the same value, compare the next set of chars in the two strings
                CharToCompare = CharToCompare + 1;
                return FindBiggerString(StrA,StrB, CharToCompare);
            }
            
            Console.WriteLine("Bigger string not found");
            return "ERROR";

        }
        
        public static int ValueChar(char CharToBeRated)
        {
            // Make char upper case as the chars below are all upper case and will not be recognised if they are lower case 
            CharToBeRated = Char.ToUpper(CharToBeRated);
            
            // If the char is a filla return 
            if (CharToBeRated == '-')
            {
                return 0;
            }
            switch (CharToBeRated)
            {
                case '0':
                    return 1;

                case '1':
                    return 2;

                case '2':
                    return 3;

                case '3':
                    return 4;

                case '4':
                    return 5;

                case '5':
                    return 6;

                case '6':
                    return 7;

                case '7':
                    return 8;

                case '8':
                    return 9;

                case '9':
                    return 10;

                case 'A':
                    return 11;

                case 'B':
                    return 12;

                case 'C':
                    return 13;

                case 'D':
                    return 14;

                case 'E':
                    return 15;

                case 'F':
                    return 16;

                case 'G':
                    return 17;

                case 'H':
                    return 18;

                case 'I':
                    return 19;

                case 'J':
                    return 20;

                case 'K':
                    return 21;

                case 'L':
                    return 22;

                case 'M':
                    return 23;

                case 'N':
                    return 24;

                case 'O':
                    return 25;

                case 'P':
                    return 26;

                case 'Q':
                    return 27;

                case 'R':
                    return 28;

                case 'S':
                    return 29;

                case 'T':
                    return 30;

                case 'U':
                    return 31;

                case 'V':
                    return 32;

                case 'W':
                    return 33;

                case 'X':
                    return 34;

                case 'Y':
                    return 35;

                case 'Z':
                    return 36;

            }

            Console.WriteLine("CHAR NOT FOUND");
            return 0;
        }

        public static string[] GetSorroundingResults(string[] Collection, int Index, int NumberOfRequestedResults)
        {
            string[] Results = new string[NumberOfRequestedResults];

            // At the top is the results its self
            Results[0] = Collection[Index];
            
            int NumberOfEachResult = (NumberOfRequestedResults - 1)/2;
            string[] ResultsBelow = new string[10];
            for (int i = 0; i < NumberOfEachResult; i++)
            {
               ResultsBelow[i] = Collection[(Index + 1) + i];
            }

            string[] ResultsAbove = new string[10];
            for (int i = 0; i < NumberOfEachResult; i++)
            {
               ResultsAbove[i] = Collection[(Index - 1) - i];
            }
            
            // Combine the two results
            int ResultsBelowBiengAdded = 0;
            int ResultsAboveBiengAdded = 0;
            bool AddResultBelow = true;
            for (int i = 1; i < NumberOfRequestedResults - 1; i++)
            {
                if (AddResultBelow)
                {
                    Results[i] = ResultsBelow[ResultsBelowBiengAdded];
                    ResultsBelowBiengAdded = ResultsBelowBiengAdded + 1;

                    AddResultBelow = false;
                }
                else
                {
                    Results[i] = ResultsAbove[ResultsAboveBiengAdded];
                    ResultsAboveBiengAdded = ResultsAboveBiengAdded + 1;

                    AddResultBelow = true;
                }
            }

            return Results;
        }
    }
}