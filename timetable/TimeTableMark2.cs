using System;
using System.Collections.Generic;

namespace timetable
{
    public class TimeTableMark2 // Simple, effecient and effective approach
    {
        public TimeTableMark2() // This class will be used to test the timetable
        {
            /*
             *
             */
            //

            // string SerializedReoccurringOccupation = "{\"ReoccuringOccupationName\":\"College\",\"DaysOfOccurrences\":[\"Tuesday\",\"Thursday\",\"Friday\"],\"TimesOfOccurrences\":[\"09/00-16/00\",\"10;45-16:15\",\"09:00-14:30\"],\"OccurrenceStringCodes\":\"19f0e75b-18da-4b24-a880-ab2ce5f9e6f1\"}";
            //    ReOc Q = JsonConvert.DeserializeObject<ReOc>(SerializedReoccurringOccupation);
            //   Event R = new Event();
            ////    R.Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 7, 00, 00, 00);
            //    R.Finish = R.Start.AddDays(7);
            //     ReOc.GetOccurncesForEvent(R);
            // ReOc Q = ReOc.CreateOccupation();

            //
            Assignment.CreateNewAssignment();

            // Test data
            // Test Occurences
            Event L = new Event();
            L.EventName = "Session";
            //     L.EventCode = Guid.NewGuid().ToString();
            L.Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 15, 30, 0);
            L.Finish = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 40, 0).AddDays(1);

            Event TimePeriod = new Event() { Start = new DateTime(1001, 1, 1, 12, 0, 0), Finish = new DateTime(1001, 1, 1, 18, 0, 0) };
            // L.SearchForTimePeriodOccurences(TimePeriod);

            // Test find comparisons
            Event J = new Event();
            J.EventName = "Session";
            //     J.EventCode = Guid.NewGuid().ToString();
            J.Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 30, 0);
            J.Finish = J.Start.AddMinutes(60);

            Event K = new Event();
            K.EventName = "Session";
            //   K.EventCode = Guid.NewGuid().ToString();
            K.Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0);
            K.Finish = K.Start.AddMinutes(60);

            J.FindTimeShared(K);
            //----
            Event AssignmentTest = new Event();
            AssignmentTest.EventName = "Assignment";
            //    AssignmentTest.EventCode = Guid.NewGuid().ToString();
            AssignmentTest.Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            AssignmentTest.Finish = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 0);
            AssignmentTest.Finish = AssignmentTest.Finish.AddDays(1);

            Event A = new Event();
            A.EventName = "Session";
            //    A.EventCode = Guid.NewGuid().ToString();
            A.Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0);
            A.Finish = A.Start.AddMinutes(60);

            Event B = new Event();
            B.EventName = "Session";
            //     B.EventCode = Guid.NewGuid().ToString();
            B.Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 13, 0, 0);
            B.Finish = B.Start.AddMinutes(60);

            Event G = new Event();
            G.EventName = "Session";
            //    G.EventCode = Guid.NewGuid().ToString();
            G.Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 17, 0, 0);
            G.Finish = G.Start.AddMinutes(120);

            Event H = new Event();
            H.EventName = "Session";
            //    H.EventCode = Guid.NewGuid().ToString();
            H.Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day /*+ 1*/ , 6, 0, 0); // adding days is done different
            H.Finish = H.Start.AddMinutes(480);

            Event C = new Event();
            C.EventName = "Session";
            //    C.EventCode = Guid.NewGuid().ToString();
            C.Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 20, 0, 0);
            C.Finish = C.Start.AddMinutes(60);

            Event M = new Event();
            M.EventName = "Session";
            //    M.EventCode = Guid.NewGuid().ToString();
            M.Start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 13, 0, 0).AddDays(1);
            M.Finish = M.Start.AddMinutes(240);

            List<Event> BookedEvents = new List<Event>();
            BookedEvents.Add(A);
            BookedEvents.Add(B);
            BookedEvents.Add(C);
            BookedEvents.Add(G);
            BookedEvents.Add(H);
            BookedEvents.Add(M);

            Session F = new Session();
            //      List<Event> D = Event.FindFreeTimesInArea(Assignment, BookedEvents);

            // Test Events class
            //    var E = B.Margin(BookedEvents);

            // Create preferences
            // These Time periods will be inorder of users preference and this is just one time period and will be used as an example
            Event DesiredTimePeriod1 = new Event() { Start = new DateTime(1001, 1, 1, 12, 0, 1), Finish = new DateTime(1001, 1, 1, 18, 0, 0) }; // Desired time period in order
            Event DesiredTimePeriod2 = new Event() { Start = new DateTime(1001, 1, 1, 18, 0, 1), Finish = new DateTime(1001, 1, 1, 00, 0, 0) }; // Desired time period in order
            Event DesiredTimePeriod3 = new Event() { Start = new DateTime(1001, 1, 1, 00, 0, 1), Finish = new DateTime(1001, 1, 1, 06, 0, 0) }; // Desired time period in order
            Event DesiredTimePeriod4 = new Event() { Start = new DateTime(1001, 1, 1, 6, 0, 1), Finish = new DateTime(1001, 1, 1, 12, 0, 0) }; // Desired time period in order
            List<Event> TimePreferences = new List<Event> { DesiredTimePeriod1, DesiredTimePeriod2, DesiredTimePeriod3, DesiredTimePeriod4 };

            // Create the session lengths4
            Event AssignmentTest2 = new Event();
            AssignmentTest2.Start = new DateTime(2019, 01, 01, 12, 00, 00);
            AssignmentTest2.Finish = new DateTime(2019, 01, 01, 16, 00, 00);

            int[] SessionLengths = new int[6] { 240, 240, 240, 240, 240, 240 };
            //     int[] BreakLengths = new int[2]{60,60};

            // var i = Session.CreateNewSessions(6, SessionLengths, /*BreakLengths,*/ BookedEvents, AssignmentTest, TimePreferences, true);
        }
    }
}