using System;
using System.Collections.Generic;
using System.Linq;

namespace timetable
{
    public class Event
    {
        #region EventProperties

        // Think about adding the padding on the sides
        public Tuple<bool, string /*Why*/> CreationReport = new Tuple<bool, string>(true, "Procedure successful");

       // public int EndPadding = 0;
     //   public int StartPadding = 0;
        public string ParentCode = "";
        public string EventName = "";
        public readonly string EventCode = "";
        public DateTime Start;
        public DateTime Finish;

        public Event()
        {
            this.EventCode = Guid.NewGuid().ToString();
        }

        public bool IsEmpty()
        {
            return (this.GetEventSize() > 0);
        }

        public int GetEventSize()
        {
            return Convert.ToInt16(Finish.Subtract(Start).TotalMinutes);
        }

        public string GetEventStatus()
        {
            if (Finish < DateTime.Now)
            {
                return "PAST";
            }
            else if (Start > DateTime.Now)
            {
                return "UPCOMING";
            }
            else if (Finish > DateTime.Now && Start < DateTime.Now)
            {
                return "INPROGRESS";
            }

            // if no condition is true
            return "UNKNOWN";
        }

        public int EventGetTimeRemaining()
        {
            if (Start > DateTime.Now)
            {
                return Convert.ToInt32(Finish.Subtract(Start).TotalMinutes);
            }
            else
            {
                return Convert.ToInt32(Finish.Subtract(DateTime.Now).TotalMinutes);
            }
        }

        private bool CheckForTimePeriod(string TimePeriodStart)
        {
            // This function return the times of day this event occur and will be used for rating
            Event TimeOfDay = new Event();

            if (TimePeriodStart == "00")
            {
                TimeOfDay = new Event() { Start = new DateTime(Start.Year, Start.Month, Start.Day, 0, 0, 0), Finish = new DateTime(Start.Year, Start.Month, Start.Day, 5, 59, 59) };
            }
            if (TimePeriodStart == "06")
            {
                TimeOfDay = new Event() { Start = new DateTime(Start.Year, Start.Month, Start.Day, 6, 0, 0), Finish = new DateTime(Start.Year, Start.Month, Start.Day, 11, 59, 59) };
            }
            if (TimePeriodStart == "12")
            {
                TimeOfDay = new Event() { Start = new DateTime(Start.Year, Start.Month, Start.Day, 12, 0, 0), Finish = new DateTime(Start.Year, Start.Month, Start.Day, 17, 59, 59) };
            }
            if (TimePeriodStart == "18")
            {
                TimeOfDay = new Event() { Start = new DateTime(Start.Year, Start.Month, Start.Day, 18, 0, 0), Finish = new DateTime(Start.Year, Start.Month, Start.Day, 23, 59, 59) };
            }

            // Check the times against the current event
            if (Clashes(new List<Event> { TimeOfDay }, new List<Event>() { this }))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private List<string> GetStringDates() // this lists all the different times and puts it into an string array and will be used when searching for times as searching through Event objects is not effeient and takes alot of resources
        {
            List<string> Times = new List<string>();
            for (int i = 0; i < this.GetEventSize() + 1; i++) // Add 1 to i because it starts at the times start at 0
            {
                // if the date only has one value add a 0 to the start
                // Put it in list so that all the values can be checked using for loop
                List<string> ThisSpecificTimeValues = new List<string>() { this.Start.AddMinutes(i).Hour.ToString(), this.Start.AddMinutes(i).Minute.ToString(), this.Start.AddMinutes(i).Day.ToString(), this.Start.AddMinutes(i).Month.ToString() };
                // Now make all the values 2 chars long or 4 for the year
                for (int j = 0; j < 4; j++)
                {
                    if (ThisSpecificTimeValues[j].Length == 1)
                    {
                        ThisSpecificTimeValues[j] = "0" + ThisSpecificTimeValues[j];
                    }
                }

                Times.Add(ThisSpecificTimeValues[0] + ThisSpecificTimeValues[1] + ThisSpecificTimeValues[2] + ThisSpecificTimeValues[3] + this.Start.AddMinutes(i).Year.ToString()); // Add an empty string to fill contain the data below
            }

            return Times;
        }

        private Event ConvertStringDatesToEvent(List<string> Times)
        {
            //10 30 28 02 2018"
            DateTime TimesEventStart = new DateTime(Convert.ToInt16(Times[0].Substring(8, 4)), Convert.ToInt16(Times[0].Substring(6, 2)), Convert.ToInt16(Times[0].Substring(4, 2)), Convert.ToInt16(Times[0].Substring(0, 2)), Convert.ToInt16(Times[0].Substring(2, 2)), 0);
            DateTime TimesEventFinish = new DateTime(Convert.ToInt16(Times[Times.Count() - 1].Substring(8, 4)), Convert.ToInt16(Times[Times.Count() - 1].Substring(6, 2)), Convert.ToInt16(Times[Times.Count() - 1].Substring(4, 2)), Convert.ToInt16(Times[Times.Count() - 1].Substring(0, 2)), Convert.ToInt16(Times[Times.Count() - 1].Substring(2, 2)), 0); ;
            return new Event() { Start = TimesEventStart, Finish = TimesEventFinish };
        }

        public static DateTime GetDateTimeFromString(string DateEntered /*DDMMYYYY*/ )
        {
            return new DateTime(Convert.ToInt16(DateEntered.Substring(6, 4)), Convert.ToInt16(DateEntered.Substring(3, 2)), Convert.ToInt16(DateEntered.Substring(0, 2)));
        }

        #endregion EventProperties

        #region EventRelationships

        public string[] FindChildren(List<Session> SearchCollection)
        {
            string[] ChildrenFound = new string[0];
            for (int i = 0; i < SearchCollection.Count; i++)
            {
                if (SearchCollection[i].ParentCode == this.EventCode)
                {
                    Array.Resize(ref ChildrenFound, ChildrenFound.Length + 1);
                    ChildrenFound[ChildrenFound.Length - 1] = SearchCollection[i].ParentCode;
                }
            }
            return ChildrenFound;
        }

        public Event FindTimeShared(Event Comparison) // this function will return the time  two events share
        {
            // Check if the two events even clash, if they dont they do not share any time
            if (!Clashes(new List<Event> { this }, new List<Event> { Comparison }))
            {
                return new Event(); // this will automatically return an empty event
            }
            // Get the times in string for the comparisons
            List<string> ThisTimes = this.GetStringDates();
            List<string> ComparisonTimes = Comparison.GetStringDates();

            // Now start to compare times
            List<string> TimesShared = ThisTimes;

            // Find the time they start sharing
            // int TimeSharingStartEleNum = 0;
            for (int i = 0; i < ThisTimes.Count; i++)
            {
                if (!ComparisonTimes.Contains(ThisTimes[i]))
                {
                    TimesShared.Remove(ThisTimes[i]);
                    i = i - 1; // Because the size of the list has decreased
                }
            }

            return ConvertStringDatesToEvent(TimesShared);
        }

        public List<Event> FindTimeSharedWithTimePeriod(Event TimePeriod) // This function will look for the occurrences of time periods in this event
        {
   
            //
            //Create all the possible time period days
            // Find all the days
            // Look through each day for the time occurence

            List<Event> PossibleTimePeriodDates = new List<Event>(); // This will be the dates used to search for the periods
            // Create the time period for the first day

            // If there is more than one day, also create the possible periods for those days
            int PossibleDaysApartStart = this.Finish.DayOfYear - this.Start.DayOfYear;
            for (int i = 0; i < PossibleDaysApartStart + 1; i++) // the one accounts for the first day and the var is for the rest of the days
            {
                DateTime PeriodOnThisDayStart = new DateTime(this.Start.Year, this.Start.Month, this.Start.Day, TimePeriod.Start.Hour, TimePeriod.Start.Minute, TimePeriod.Start.Second).AddDays(i);
                DateTime PeriodOnThisDayFinish = PeriodOnThisDayStart.AddHours(6);

                PossibleTimePeriodDates.Add(new Event() { Start = PeriodOnThisDayStart, Finish = PeriodOnThisDayFinish });
            }

            // Look for these time occurrences in the events
            List<Event> PeriodOccurences = new List<Event>();
            for (int i = 0; i < PossibleTimePeriodDates.Count; i++)
            {
                PeriodOccurences.Add(this.FindTimeShared(PossibleTimePeriodDates[i])); // DO THIS IN FINDBESTSLOT FUNCTION TO AVOID SPEGHETTI CODE
            }

            // Remove the period that do not share a time with the this event as this even happens on the same day but start after it or finishes before it. If this is true the time shared with the occurence  will be 0
            for (int i = 0; i < PeriodOccurences.Count; i++)
            {
                if (PeriodOccurences[i].GetEventSize() <= 0)
                {
                    PeriodOccurences.RemoveAt(i);
                    i = i - 1;
                }
            }
            return PeriodOccurences;
        }

    
        public Tuple<int, int> Margin(List<Event> Events) // Return the number of free space sorrounding this event
        {
            // NOTE: Pass in all events including task events

            // Firstly remove this event from the list of events so that it doesnt get compared with its self and only gets compared with the events around
            for (int i = 0; i < Events.Count; i++)
            {
                if (Events[i].EventCode == this.EventCode)
                {
                    Events.RemoveAt(i);
                    break;
                }
            }

            // Keep on checking left until a clash occurs
            bool ClosesEventToLeftFound = false;

            int LeftMargin = 0;

            while (!ClosesEventToLeftFound)
            {
                // Go a minute back until a clash occurs
                if (Clashes(new List<Event> { new Event() { Start = this.Start.AddMinutes(LeftMargin), Finish = this.Finish } }, Events))
                {
                    ClosesEventToLeftFound = true;
                }
                else
                {
                    LeftMargin--;
                }
            }

            int RightMargin = 0;
            // Keep on checking left until a clash occurs
            bool ClosesEventToRightFound = false;
            while (!ClosesEventToRightFound)
            {
                if (Clashes(new List<Event> { new Event() { Start = this.Start, Finish = this.Finish.AddMinutes(RightMargin) } }, Events))
                {
                    ClosesEventToRightFound = true;
                }
                else
                {
                    RightMargin++;
                }
            }

            return new Tuple<int, int>(LeftMargin, RightMargin);
        }

        public static List<Event> SortEvents(List<Event> EventsToSort)
        {
            Event TempAreaHolder;
            for (int i = 0; i < EventsToSort.LongCount(); i++)
            {
                for (int j = 0; j < EventsToSort.LongCount(); j++)
                {
                    if (EventsToSort[i].GetEventSize() > EventsToSort[j].GetEventSize())
                    {
                        TempAreaHolder = EventsToSort[i];
                        EventsToSort[i] = EventsToSort[j];
                        EventsToSort[j] = TempAreaHolder;
                    }
                }
            }

            return EventsToSort;
        }

        public static List<Event> FilterEvents(int RequiredSize, List<Event> Events)
        {
            // Remove all the areas that are too small for this session
            for (int i = 0; i < Events.LongCount(); i++)
            {
                if (Events[i].GetEventSize() < RequiredSize) // Get rid of any areas that are
                {
                    Events.RemoveAt(i);
                    i = i - 1;
                }
            }

            // return areas
            return Events;
        }

        public static List<Event> FindOpenTimesInArea(Event SearchArea, List<Event> BookedEvents) // this function will return the empty
        {
            List<Event> Areas = new List<Event>();

            //DateTime[] AreasStart = new DateTime[1];
            //DateTimbe[] AreasFinish = new DateTime[1];

            int Area = 0;
            // Add area 0
            Areas.Add(new Event());
            // First area starts on the LaunchDate
            Areas[Area].Start = SearchArea.Start;
            Areas[Area].Finish = Areas[Area].Start;
            while (Areas[Area].Finish < SearchArea.Finish)
            {
                // If adding a minutes to the length of the area creates no clashes then add the minute
                List<Event> ThisAreaInList = new List<Event>();
                ThisAreaInList.Add(Areas[Area]);
                if (!Event.Clashes(BookedEvents, ThisAreaInList)) //new DateTime[1] { Areas[Area].Start }, new DateTime[1] { Areas[Area].Finish.AddMinutes(1) }))
                {
                    Areas[Area].Finish = Areas[Area].Finish.AddMinutes(1);
                }
                else // If doing so  create clashes
                {
                    // if the length of the area is more than 1 minute than move onto the next area otherwise, just recreate the current area as it is not long enough
                    if (Areas[Area].GetEventSize() > 0)
                    {
                        Area = Area + 1;

                        // Create the new area
                        // Array.Resize(ref AreasStart, AreasStart.Length + 1);
                        // Array.Resize(ref AreasFinish, AreasFinish.Length + 1);

                        // Start the new area one minute after the old area
                        Areas.Add(new Event());
                        Areas[Area].Start = Areas[Area - 1].Finish.AddMinutes(1);
                        Areas[Area].Finish = Areas[Area].Start;
                    }
                    else
                    {
                        // Start the new area one minute after the current area as it is too small or is completey blocked
                        Areas[Area].Start = Areas[Area].Finish.AddMinutes(1);
                        Areas[Area].Finish = Areas[Area].Start;
                    }
                }
            }

            // if the final area is smaller than 1, it will stay in the array of areas because the else statement which removes it is not executed as the while loop has ended
            if (Areas[Areas.Count - 1].GetEventSize() < 1)
            {
                Areas.RemoveAt(Areas.Count - 1);
            }

            return Areas;
        }

        public static bool Clashes(List<Event> A, List<Event> B)
        {
            // MAKES THE NOTES FOR THIS ############################################
            // Check if the new session clashes with any current sessions
            for (int i = 0; i < A.Count; i++)
            {
                for (int j = 0; j < B.Count; j++)
                {
                    if (A[i].Start == B[j].Start && A[i].Finish == B[j].Finish)
                    {
                        return true;
                    }
                    if (!(A[i].Start > B[j].Start && A[i].Start > B[j].Finish /*NEW EV IS BEFORE OCCU*/ || A[i].Start < B[j].Start && A[i].Finish < B[j].Start /*NEW EV IS AFTER OCCU*/ ))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion EventRelationships
    }
}