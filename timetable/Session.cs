using System;
using System.Collections.Generic;

namespace timetable
{
    public class Session : Event // Used to create, monipulate test, and view session events
    {
        #region SessionProperties

        //        int BreakBeforeSession = 0;
        //        int BreakAfterSession = 0;

        #endregion SessionProperties

        #region CreateASession

        /* Adding a new session will be done by creating an event that fits preference
        if the best suitable event is before current sessions current then just create a new event and push the session details backwards one session and put the new session in at the new event that has been emptied up (check Diagram )
        */

        // Create a quick check function that checks whether the requested number of sessions can be created before the time starts
        public static Tuple<string, List<Session>> CreateNewSessions(int NumberOfSessions, int[] SessionSizes, /*int[] BreakAfterSessions*/ List<Event> BookedEvents, Event TimerPeriodForSessions, List<Event> TimesInOrderOfPreference, bool AreaDesiredOverPeriod)
        {
            int MaxSessoinSize = TimerPeriodForSessions.GetEventSize(); // Make this value the biggest it can be

            int FailedCreations = 0;
            List<Session> CreatedSessions = new List<Session>();
            for (int i = 0; i < NumberOfSessions; i++)
            {
                Session CreatedSession;
                if (SessionSizes[i] /*+ BreakAfterSessions[i]*/ <= MaxSessoinSize)
                {
                    // Only create a session if their is enough space for it
                    CreatedSession = CreateSession(SessionSizes[i], /*BreakAfterSessions[i],*/ BookedEvents, TimerPeriodForSessions, TimesInOrderOfPreference, AreaDesiredOverPeriod);
                    if (CreatedSession.CreationReport.Item1)
                    {
                        CreatedSessions.Add(CreatedSession);
                        // Add this session to the bespoke events
                        // Convert sesssion into an event
                        BookedEvents.Add(CreatedSession);
                    }
                    // If a session fails to be created it means their is no space for it. If it is because their isn't enough time, then any sessions that is the same size will not be attempted to be created
                    else
                    {
                        // Show that this session failed to be created
                        CreatedSession = new Session() { CreationReport = new Tuple<bool, string>(false, "Not enough space") };
                        CreatedSessions.Add(CreatedSession);

                        // Because this sessoin failed to create because of its size, make the maximum session size smaller then this sessions size
                        MaxSessoinSize =/*(*/ SessionSizes[i] /*+ BreakAfterSessions[i])*/ - 1;

                        // Add one to the number of failed creations
                        FailedCreations++;
                    }
                }
                else
                {
                    CreatedSession = new Session() { CreationReport = new Tuple<bool, string>(false, "Not enough space") };
                    CreatedSessions.Add(CreatedSession);
                    FailedCreations++;
                }
            }

            return new Tuple<string, List<Session>>("Successfully Created: " + (NumberOfSessions - FailedCreations).ToString() + "/" + NumberOfSessions.ToString(), CreatedSessions);
        }

        private static Session CreateSession(int SessionsSize, /*BreakAfterSessions[i],*/ List<Event> BookedEvents, Event Task, List<Event> TimesInOrderOfPreference, bool AreaDesiredOverPeriod)
        {
            // Get all possible slots
            // Select one using the ratings
            // Create the session
            // Return it

            // Get the areas available
            List<Event> Areas = Event.FindOpenTimesInArea(Task, BookedEvents); // This is all the open areas

            // Filter the events and leave the events big enough to hold a session
            Areas = Event.FilterEvents(SessionsSize /*+ BreakAfterSession*/, Areas);

            // Sort the areas
            Areas = Event.SortEvents(Areas);

            /*
                If the largest available area is smaller then the required session size
             */

            // Get the slot
            if (AreaDesiredOverPeriod)
            {
                // Look through all the areas until the right period is found

                Event SlotFound = FindBestPeriodInBiggestArea(Areas, TimesInOrderOfPreference, SessionsSize /*+ BreakAfterSession*/);
                // Place session inside slot
                if (SlotFound.CreationReport.Item1) // If the even has been created
                {
                    // Create the sessions
                    Session NewSession = PlaceSessionInSlot(SlotFound, SessionsSize /*+ BreakAfterSession*/);
                    // Add other properties to session
                    //NewSession.BreakAfterSession = BreakAfterSession;
                    NewSession.ParentCode = Task.EventCode;

                    return NewSession;
                }
                else
                {
                    // return why it couldnt be created
                    return new Session() { CreationReport = SlotFound.CreationReport };
                }
            }
            else
            {
                //...
            }

            return new Session();
        }

        private static Event FindBestPeriodInBiggestArea(List<Event> Areas, List<Event> TimesInOrderOfPreference, int NetSessionSize)
        {
            // Check using the time period
            // return the biggest the biggest area with the

            // Look for a time in the biggest times
            // Look through all areas in area of biggest
            for (int i = 0; i < Areas.Count; i++)
            {
                // Look for a slot in the times
                for (int j = 0; j < TimesInOrderOfPreference.Count; j++)
                {
                    // Check wehther this area has
                    // Find all period occurences
                    // Return the biggest one

                    // Find all times this period occurs in this area
                    List<Event> TimePeriodOccurencesFound = Areas[i].FindTimeSharedWithTimePeriod(TimesInOrderOfPreference[j]);

                    // Check
                    //Event Session = PlaceSessionInArea(Areas[i], TimesInOrderOfPreference[j], SessionNetSize);

                    // Sort the found occurences
                    TimePeriodOccurencesFound = Event.SortEvents(TimePeriodOccurencesFound);

                    // If the number of periods found is null (empty) skip over anyway
                    if (TimePeriodOccurencesFound.Count > 0)
                    {
                        // If the biggest period is too small return event not found
                        if (TimePeriodOccurencesFound[0].GetEventSize() >= NetSessionSize)
                        {
                            return TimePeriodOccurencesFound[0];
                        }
                    }

                    // If the biggest occurence of this perference is too small look for the next preference
                }
            }

            // if the number of areas is 0 or the number of period occurences is 0
            return new Event() { CreationReport = new Tuple<bool, string>(false, "No areas is suitable or availables") }; // Switch over if else for readability
        }

        private static Session PlaceSessionInSlot(Event SlotToPlaceSessionIn, int SessionNetSize)
        {
            // USER WILL GET TO CHOOSE WHETHER THEY WOULD RATHER HAVE THE TIME OF DAY THEY CHOSE OR THE LARGE SPACE BETWEEN THE SESSION. Either way, the program will find both if it can but if one is present in a place where the other is not the users favourite preference (either time of day or space inbetween sessions)
            /*
            1. The program looks for the preferred time in the largest area
            2. The user will now have the option of Choosing a different preffered time so that they get to try again for a session in the largest area which will have the most free time
            3. OR they can choose to pick the second biggest area so that they still get to try for their favourite time
            */

            // if it is large enough, place a session right in the centre so that there is extra space on either side
            // Find the extra amount time
            int ExtraTime = SlotToPlaceSessionIn.GetEventSize() - SessionNetSize;
            // Divide it by two to put the sesson right in the middle
            int StartPadding = ExtraTime / 2;
            int EndPadding = ExtraTime / 2;

            // Creat the session
            Session NewSession = new Session()
            {
                Start = SlotToPlaceSessionIn.Start.AddMinutes(StartPadding),
                Finish = SlotToPlaceSessionIn.Finish.AddMinutes(-EndPadding)
            };

            return NewSession;
        }

        #endregion CreateASession
    }
}