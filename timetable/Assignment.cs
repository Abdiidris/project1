using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace timetable
{
    public class Assignment : Event
    {
        #region NOTES

        /*
         * Make it so that days that have already been passed are not used in the session creation when a new assignment has alread been started
        */

        #endregion NOTES

        // Enter Assignment Details
        /*
        Session partition will breaking up the
        */

        private List<Event> Tasks = new List<Event>();
        private List<Event> Sessions = new List<Event>();

        public int GetTaskCount()
        {
            return Tasks.Count;
        }

        public static Assignment CreateNewAssignment()
        {
            Assignment NewAssignment = new Assignment();

            // Assignment Name
            //--SPEEDTEST--//Console.WriteLine("Assignment Name");
            NewAssignment.EventName = "Test";
            //--SPEEDTEST--//NewAssignment.EventName = Console.ReadLine();

            // Assignment Start and Finish date
            NewAssignment.Start = AskUserForAppropriateDate("Assignment Start Date");
            NewAssignment.Finish = AskUserForAppropriateDate("Assignment FinishDate");
            //NewAssignment.Start = new DateTime(2018, 05, 14);
            //NewAssignment.Finish = new DateTime(2018, 05, 17);

            Console.Clear();
            //--SPEEDTEST--//if (NewAssignment.Finish < DateTime.Now)
            //--SPEEDTEST--//{
            //--SPEEDTEST--//Console.WriteLine("This assignment has already been passed");
            //--SPEEDTEST--//Console.ReadLine();
            //--SPEEDTEST--//return Assignment.CreateNewAssignment();
            //--SPEEDTEST--//}

            //--SPEEDTEST--//NewAssignment = ModifyAndShowAssignment(NewAssignment);
            // Create tasks
            //--SPEEDTEST--//Console.WriteLine("How many tasks");
            string SampleTask = "{\"Item1\":[{\"CreationReport\":{\"Item1\":true,\"Item2\":\"Procedure successful\"},\"ParentCode\":\"\",\"EventName\":\"Assignment 1: Task 1\",\"EventCode\":\"81d07b2e-4ace-4dcc-9f29-3513c72e5a1f\",\"Start\":\"0001-01-01T00:00:00\",\"Finish\":\"0001-01-01T00:00:00\"},{\"CreationReport\":{\"Item1\":true,\"Item2\":\"Procedure successful\"},\"ParentCode\":\"\",\"EventName\":\"Assignment 1: Task 2\",\"EventCode\":\"bac1df7e-1934-45ed-8e0e-a6eb62e96f37\",\"Start\":\"0001-01-01T00:00:00\",\"Finish\":\"0001-01-01T00:00:00\"},{\"CreationReport\":{\"Item1\":true,\"Item2\":\"Procedure successful\"},\"ParentCode\":\"\",\"EventName\":\"Assignment 1: Task 3\",\"EventCode\":\"573a1258-a745-45c7-9970-e57dedd65f48\",\"Start\":\"0001-01-01T00:00:00\",\"Finish\":\"0001-01-01T00:00:00\"},{\"CreationReport\":{\"Item1\":true,\"Item2\":\"Procedure successful\"},\"ParentCode\":\"\",\"EventName\":\"Assignment 1: Task 4\",\"EventCode\":\"8bfaf205-49ca-4c24-b546-f1276c72161a\",\"Start\":\"0001-01-01T00:00:00\",\"Finish\":\"0001-01-01T00:00:00\"}],\"Item2\":[[240,240],[240,240],[240,240,240],[240]]}";
            Tuple<List<Event>, List<int[]>> TaskInformaiton = JsonConvert.DeserializeObject<Tuple<List<Event>, List<int[]>>>(SampleTask);//--SPEEDTEST--//CreateTaskInformationAndSize(NewAssignment);

           NewAssignment.Tasks = TaskInformaiton.Item1;

            string TaskInfoJson = JsonConvert.SerializeObject(TaskInformaiton);

            NewAssignment.CreateSessionsForNewAssignment(TaskInformaiton.Item2);

            return new Assignment();
        }

        public void CreateSessionsForNewAssignment(List<int[]> SessionLengthsForTask)
        {
            // MOVE THIS INTO A FUNCTION
            List<Event> AssignmentParts = this.BreakUpAssignmentForSessions(this, SessionLengthsForTask);
            // Create the sessions
            // for testing and instead will be asked of the user
            Event DesiredTimePeriod1 = new Event() { Start = new DateTime(1001, 1, 1, 12, 0, 1), Finish = new DateTime(1001, 1, 1, 18, 0, 0) }; // Desired time period in order
            Event DesiredTimePeriod2 = new Event() { Start = new DateTime(1001, 1, 1, 18, 0, 1), Finish = new DateTime(1001, 1, 1, 00, 0, 0) }; // Desired time period in order
            Event DesiredTimePeriod3 = new Event() { Start = new DateTime(1001, 1, 1, 00, 0, 1), Finish = new DateTime(1001, 1, 1, 06, 0, 0) }; // Desired time period in order
            Event DesiredTimePeriod4 = new Event() { Start = new DateTime(1001, 1, 1, 6, 0, 1), Finish = new DateTime(1001, 1, 1, 12, 0, 0) }; // Desired time period in order
            List<Event> TimePreferences = new List<Event> { DesiredTimePeriod1, DesiredTimePeriod2, DesiredTimePeriod3, DesiredTimePeriod4 };

            List<Tuple<string, List<Session>>> CreatedSessionsList = new List<Tuple<string, List<Session>>>();
            for (int i = 0; i < AssignmentParts.Count; i++)
            {
                // Get all the relevant reoccurringOccupations
                List<Event> AllRelReOcs = ReOc.GetOccurncesForEvent(AssignmentParts[i]);
                CreatedSessionsList.Add(Session.CreateNewSessions(SessionLengthsForTask[i].Length/*NumberOfSessionsForThisTask*/, SessionLengthsForTask[i], AllRelReOcs, AssignmentParts[i], TimePreferences, true));
                System.Console.WriteLine(CreatedSessionsList[i].Item1);
            }

            for (int i = 0; i < this.Tasks.Count; i++)
            {
                // First display the task information
                Console.WriteLine(i.ToString() + ". " + this.Tasks[i].EventName);
                for (int j = 0; j < CreatedSessionsList[i].Item2.Count; j++)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    // Show the name and the number
                    // Set the event name
                    CreatedSessionsList[i].Item2[j].EventName = this.Tasks[i].EventName + ": Session " + (j + 1).ToString();
                    System.Console.WriteLine("  " + (j + 1).ToString() + ". " + CreatedSessionsList[i].Item2[j].EventName);

                    // Show the start and the finish
                    System.Console.WriteLine("      Start:   " + CreatedSessionsList[i].Item2[j].Start.ToString());
                    System.Console.WriteLine("      Finish:  " + CreatedSessionsList[i].Item2[j].Finish.ToString());

                    Console.ForegroundColor = ConsoleColor.Cyan;
                }
            }
        }

        private List<Event> BreakUpAssignmentForSessions(Assignment NewAssignment, List<int[]> NumberOfMinutesPerSession)
        {
            // Each item of the list of integers belongs to the next task
            // Each in the integers represents a sessions length

            // Create all the weights for all the different tasks
            // First work out the total session time
            int TotalSessionTime = 0;
            for (int i = 0; i < NumberOfMinutesPerSession.Count; i++)
            {
                for (int j = 0; j < NumberOfMinutesPerSession[i].Length; j++)
                {
                    TotalSessionTime = TotalSessionTime + NumberOfMinutesPerSession[i][j];
                }
            }

            double[] TaskWeights = new double[NumberOfMinutesPerSession.Count];
            for (int i = 0; i < NumberOfMinutesPerSession.Count; i++)
            {
                double TotalTaskSessionWeights = 0.0;
                for (int j = 0; j < NumberOfMinutesPerSession[i].Length; j++)
                {
                    TotalTaskSessionWeights = TotalTaskSessionWeights + NumberOfMinutesPerSession[i][j];
                }
                TaskWeights[i] = ((TotalTaskSessionWeights / TotalSessionTime));
            }
            // Break the assignment using the TaskWeightPercentages and then return the new parts
            List<Event> AssignmentParts = new List<Event>();
            // Set up the first assignment part
            // Get this part of the assignment
            AssignmentParts.Add(new Event());
            // Workout this tasks part at the assignment
            int FirstTasksAssignmentPartLength = Convert.ToInt16(TaskWeights[0] * NewAssignment.GetEventSize());
            AssignmentParts[0].Start = NewAssignment.Start;
            AssignmentParts[0].Finish = AssignmentParts[0].Start.AddMinutes(FirstTasksAssignmentPartLength);

            for (int i = 1; i < TaskWeights.Length; i++)
            {
                AssignmentParts.Add(new Event());
                // Workout this tasks part at the assignment
                int ThisTasksAssignmentPartLength = Convert.ToInt16(TaskWeights[i] * NewAssignment.GetEventSize());

                AssignmentParts[i].Start = AssignmentParts[i - 1].Finish.AddMinutes(+1);
                AssignmentParts[i].Finish = AssignmentParts[i].Start.AddMinutes(ThisTasksAssignmentPartLength);
            }

            return AssignmentParts;
        }

        public static Assignment ModifyAndShowAssignment(Assignment NewAssignment)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Assignment-------------------------------------------------");
            // Show assignment
            Console.WriteLine("1. Name - " + NewAssignment.EventName);
            Console.WriteLine("|------|");
            Console.WriteLine("2. Start - " + NewAssignment.Start.ToString());
            Console.WriteLine("|------|");
            Console.WriteLine("3. Finish - " + NewAssignment.Finish.ToString());
            Console.WriteLine("|------|--|------|--|------|");
            Console.WriteLine("Specify Edit or write con to continue");
            string Input = Console.ReadLine();

            switch (Input)
            {
                case "1":
                    Console.WriteLine("Assignment Name");
                    NewAssignment.EventName = Console.ReadLine();
                    return ModifyAndShowAssignment(NewAssignment);

                case "2":
                    NewAssignment.Start = AskUserForAppropriateDate("Assignment Start Date");
                    return ModifyAndShowAssignment(NewAssignment);

                case "3":
                    NewAssignment.Finish = AskUserForAppropriateDate("Assignment Finish Date");
                    return ModifyAndShowAssignment(NewAssignment);

                case "con":
                    Console.Clear();
                    return NewAssignment;

                default:
                    return ModifyAndShowAssignment(NewAssignment);
            }
        }

        // private static List<Event> CreatTaskTimes(int[] TaskWeights, List<Event> TasksSS, Assignment NewAssignment)
        // {
        //     int NumberOfTasks = 0;
        //     // Add functionality that lets you choose the percentage size of each session

        //     // Create Generic
        //     List<Event> Tasks = new List<Event>();

        //     int EachTaskLength = NewAssignment.EventGetTimeRemaining() / NumberOfTasks;

        //     // Create the first task
        //     // Each task deadline will be equally divided
        //     Event TaskOne = new Event();
        //     if (NewAssignment.GetEventStatus() == "INPROGRESS")
        //     {
        //         TaskOne.Start = DateTime.Now;
        //         TaskOne.Finish = TaskOne.Start.AddMinutes(EachTaskLength);

        //     }
        //     else if (NewAssignment.GetEventStatus() == "UPCOMING")
        //     {
        //         TaskOne.Start = DateTime.Now;
        //         TaskOne.Finish = TaskOne.Start.AddMinutes(EachTaskLength);
        //     }

        //     // Add the new task to the list
        //     Tasks.Add(TaskOne);

        //     for (int i = 1; i < NumberOfTasks; i++)
        //     {
        //         Event NextTask = new Event();
        //         NextTask.Start = Tasks[i - 1].Finish;
        //         NextTask.Finish = NextTask.Start.AddMinutes(EachTaskLength);

        //         // Add generic task information
        //         Tasks.Add(NextTask);

        //     }

        //     return Tasks;
        // }

        public static Tuple<List<Event>, List<int[]>> CreateTaskInformationAndSize(Assignment NewAssignment)
        {
            /*
            The function will get set the name for each task
            The wieght of each task will also be set
            */

            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Cyan;
            System.Console.WriteLine("Task-------------------------------------------------");
            //--
            List<Event> Tasks = new List<Event>();
            //--
            Console.WriteLine("How many tasks are their for this assignment");
            int NumberOfTasks = Convert.ToInt16(Console.ReadLine());
            //--
            List<int[]> SessionSizes = new List<int[]>();
            //int[] SessionSizes = new int[NumberOfTasks];
            for (int i = 0; i < NumberOfTasks; i++)
            {
                Console.Clear();
                // Create the new task
                Tasks.Add(new Event());
                // Get the name for the task
                Console.WriteLine("|------| Task: " + (i + 1).ToString());
                System.Console.WriteLine("Enter gen for a generic value");
                Console.WriteLine("Task name");
                //      if (Console.ReadLine() == "gen")
                //    {
                Tasks[i].EventName = NewAssignment.EventName + ": Task " + (i + 1).ToString();
                System.Console.WriteLine(Tasks[i].EventName);
                //  }

                // Ask for the session information
                System.Console.WriteLine("Session----------------------------");
                System.Console.WriteLine("How many sessions for this task");
                int NumberOfSessionForThisTask = Convert.ToInt16(Console.ReadLine());
                // Get each individual session size
                // Add a new item to the int array list so that the times for this task can be stored
                SessionSizes.Add(new int[NumberOfSessionForThisTask]);
                for (int j = 0; j < NumberOfSessionForThisTask; j++)
                {
                    Console.WriteLine(Tasks[i].EventName + ": Session " + j.ToString() + "-------");
                    Console.WriteLine("What would you like the sessoins size to be HH/MM");
                    string SelectedTime = Console.ReadLine();
                    SessionSizes[i][j] = (Convert.ToInt16(SelectedTime.Substring(0, 2)) * 60) + Convert.ToInt16(SelectedTime.Substring(3, 2));
                }
            }

            Console.Clear();
            // Display the tasks
            for (int i = 0; i < NumberOfTasks; i++)
            {
                Console.WriteLine("Name : " + Tasks[i].EventName);
            }

            System.Console.WriteLine("type restart to start again or press any other key to continue ");

            if (Console.ReadLine() == "restart")
            {
                return CreateTaskInformationAndSize(NewAssignment);
            }
            else
            {
                return new Tuple<List<Event>, List<int[]>>(Tasks, SessionSizes);
            }
        }

        public static DateTime AskUserForAppropriateDate(string DateName)
        {
            Console.WriteLine("Enter '" + DateName + "' ( DD/MM/YYYY E.g. 03/04/2018 ) ");
            string StringDate = Console.ReadLine();
            if (StringDate.Length != 10)
            {
                Console.WriteLine("You entered something wrong");
                return AskUserForAppropriateDate(DateName);
            }
            else
            {
                return Event.GetDateTimeFromString(StringDate);
            }
        }
    }
}