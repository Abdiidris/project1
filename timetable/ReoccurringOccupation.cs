﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace timetable
{
    internal class ReOc // ReoccuringOccupation
    {
        public string ReoccuringOccupationName = "";
        public string[] DaysOfOccurrences = new string[0];
        public string[] TimesOfOccurrences;
        public string ReOcCode;
        public bool ReoccuringOccupationLive = true;

        public ReOc()
        {
            ReOcCode = Guid.NewGuid().ToString();
        }

        public static ReOc CreateOccupation()
        {
            ReOc NewReOc = new ReOc();
            Console.WriteLine("What is the occupations name");
            NewReOc.ReoccuringOccupationName = Console.ReadLine();
            // Get the days they occur
            Console.WriteLine("y iF the event occurs on that day and n if it does not");
            string[] DifferentDaysOfTheWeek = new string[7] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
            for (int i = 0; i < 7; i++)
            {
                Console.WriteLine((i + 1).ToString() + ". " + DifferentDaysOfTheWeek[i]);

                if (Console.ReadLine() == "y")
                {
                    Array.Resize(ref NewReOc.DaysOfOccurrences, NewReOc.DaysOfOccurrences.Length + 1);

                    NewReOc.DaysOfOccurrences[NewReOc.DaysOfOccurrences.Length - 1] = DifferentDaysOfTheWeek[i].ToString();
                }
            }

            // Change the size for the times they occur
            NewReOc.TimesOfOccurrences = new string[NewReOc.DaysOfOccurrences.Length];
            // Get the different times they occur on that those days
            for (int i = 0; i < NewReOc.DaysOfOccurrences.Length; i++)
            {
                Console.WriteLine("Write the times they will occur HH/MM");
                Console.WriteLine(NewReOc.DaysOfOccurrences[i]);
                Console.WriteLine("Start");
                NewReOc.TimesOfOccurrences[i] = Console.ReadLine();
                Console.WriteLine("Length");
                NewReOc.TimesOfOccurrences[i] = NewReOc.TimesOfOccurrences[i] + "-" + Console.ReadLine();
                Console.Clear();
            }

            // Save it before returning it
            SaveReoccuringOccupation(NewReOc);
            Console.WriteLine("ReoccurringOccupatoin Saved");
            return NewReOc;
        }

        public static void SaveReoccuringOccupation(ReOc reOc)
        {
            // Serialize the object
            File.WriteAllText("data/SerializedObjects/ReoccurringEvents/" + reOc.ReOcCode + ".txt ", JsonConvert.SerializeObject(reOc));
            // Add this code to the index
            // Get current index and add new index
            string[] NewIndex = File.ReadAllLines("data/SerializedObjects/ReoccurringEvents/index.txt");
            Array.Resize(ref NewIndex, NewIndex.Length + 1);
            NewIndex[NewIndex.Length - 1] = reOc.ReOcCode;
            File.WriteAllLines("data/SerializedObjects/ReoccurringEvents/index.txt", NewIndex);
        }

        public static List<ReOc> GetAll()
        {
            string[] AllReOcCodes = File.ReadAllLines("data/SerializedObjects/ReoccurringEvents/index.txt");
            List<ReOc> AllStoredReOcs = new List<ReOc>();
            for (int i = 0; i < AllReOcCodes.Length; i++)
            {
               AllStoredReOcs.Add(JsonConvert.DeserializeObject<ReOc>(File.ReadAllText("data/SerializedObjects/ReoccurringEvents/" + AllReOcCodes[i] + ".txt")));
            }

            return AllStoredReOcs;
        }

        public static List<Event> GetOccurncesForEvent(Event EventForOccurrences)
        {
            // First get all the ReOcs
            List<ReOc> AllReOcs = GetAll();

            List<Event> OccupationOccurrencesWithinEvent = new List<Event>();
            for (int h = 0; h < AllReOcs.Count; h++)
            {
                // Check each day event occurs to see if this occupation also occurs on that day
                int NumberOfDaysEventOccurs = EventForOccurrences.Finish.DayOfYear - EventForOccurrences.Start.DayOfYear;

                //string[] DaysEventOccurs = new string[NumberOfDaysEventOccurs];
                for (int i = 0; i < NumberOfDaysEventOccurs; i++)
                {
                    // Check this day against all the days the occupation occurs
                    for (int j = 0; j < AllReOcs[h].DaysOfOccurrences.Length; j++)
                    {
                        if (AllReOcs[h].DaysOfOccurrences[j] == EventForOccurrences.Start.AddDays(Convert.ToDouble(i)).DayOfWeek.ToString())
                        {
                            // Create the event for this occurence
                            OccupationOccurrencesWithinEvent.Add(new Event());
                            OccupationOccurrencesWithinEvent[OccupationOccurrencesWithinEvent.Count - 1].EventName = AllReOcs[h].ReoccuringOccupationName + " Occurrence";
                            OccupationOccurrencesWithinEvent[OccupationOccurrencesWithinEvent.Count - 1].Start = new DateTime(EventForOccurrences.Start.Year, EventForOccurrences.Start.Month, EventForOccurrences.Start.Day, Convert.ToInt16(AllReOcs[h].TimesOfOccurrences[j].Substring(0, 2)), Convert.ToInt16(AllReOcs[h].TimesOfOccurrences[j].Substring(3, 2)), 00).AddDays(i);
                            // Get the length of this occupation in minutes
                            int OccupationLengthInMinutes = (Convert.ToInt16(AllReOcs[h].TimesOfOccurrences[j].Substring(6, 2)) * 60) + Convert.ToInt16(AllReOcs[h].TimesOfOccurrences[j].Substring(9, 2));
                            OccupationOccurrencesWithinEvent[OccupationOccurrencesWithinEvent.Count - 1].Finish = OccupationOccurrencesWithinEvent[OccupationOccurrencesWithinEvent.Count - 1].Start.AddMinutes(OccupationLengthInMinutes);
                        }
                    }
                }
            }

            return OccupationOccurrencesWithinEvent;
        }
    }
}